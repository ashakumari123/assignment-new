import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm:FormGroup

  constructor(private fb:FormBuilder,
    private _US:UserService,
    private router:Router) { }

  ngOnInit() {
    this.loginForm=this.fb.group({
      email:["",Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
      password:["",Validators.required]
    })
  }

  login(){
   if(this.loginForm.valid){
    this._US.loginUser(this.loginForm.value).subscribe((res:any)=>{
      if(res.token){
        this._US.openSnackBar("login successfully")
        localStorage.setItem("token",res.token)
        this._US.loginSubject.next(true)
        this.router.navigate(['/users'])
      }
      else{
        this._US.openSnackBar("incorrect username or password")
      }
    },(err)=>{
      this._US.openSnackBar("incorrect username or password")

    })
   }
   else{
     this.loginForm.markAllAsTouched()
   }
  }
}
