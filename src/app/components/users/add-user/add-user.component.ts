import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  addUserForm:FormGroup
  userId:any=''

  constructor(private fb:FormBuilder,
    private _US:UserService,
    private router:Router,
    private route:ActivatedRoute) { }

  ngOnInit() {
    this.formControl()
    this.route.queryParams.subscribe(param=>{
      this.userId=param.id
      if(this.userId){
        this.getUserDetail()
      }
      else{
        this.userId = ''
        this.addUserForm.reset()
      }
    })
   }
   
  formControl(){
    this.addUserForm = this.fb.group({
      first_name:["",Validators.required],
      last_name:["",Validators.required],
      email:["",Validators.compose([Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
      pincode:["",Validators.required],
      username:["",Validators.required],
      address:["",Validators.required],
      mobile:["",Validators.required]
    })
  }

  addUser(){
   if(this.addUserForm.valid){
    this._US.createUser(this.addUserForm.value).subscribe((res:any)=>{
      if(res){
        this._US.openSnackBar("user created successfully")
        this.router.navigate(['/users'])
      }
      else{
        this._US.openSnackBar("unbale to create user")
      }
    })
   }
   else{
     this.addUserForm.markAllAsTouched()
   }
  }

  getUserDetail(){
    this._US.getSignleUser(this.userId).subscribe((res:any)=>{
      if(res){
        this.addUserForm.patchValue(res.data)
      }
    })
  }

  updateUser(){
   if(this.addUserForm.valid){
    this._US.updateUser(this.userId,this.addUserForm.value).subscribe((res:any)=>{
      if(res){
        this._US.openSnackBar("user updated successfully")
        this.router.navigate(['/users'])
      }
      else{
        this._US.openSnackBar("unbale to create user")
      }
    })
   }
   else{
     this.addUserForm.markAllAsTouched()
   }
  }
}
