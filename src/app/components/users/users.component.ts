import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  displayedColumns: string[] = ['position', 'image', 'name', 'weight', 'symbol'];
  dataSource = []

  params: any = {
    per_page:5
  }

  constructor(private _US: UserService) { }

  ngOnInit() {
    this.getUsers()
  }

  getUsers() {
    this._US.getUsers(this.params).subscribe((res: any) => {
      this.dataSource = res.data
    })
  }

  loadData(event) {
    if(event){
      if(event.pageSize>5){
        this.params.per_page=event.pageSize
      }
      else if(event.pageIndex == 0){
        this.params.per_page=5
      }
      else if(event.pageIndex == 1){
        this.params.per_page=10
      }
      else if(event.pageIndex == 2){
        this.params.per_page=15
      }
    }
    this.getUsers()

  }

  deleteUser(id){
    this._US.deleteUser(id).subscribe((res:any)=>{
      if(res){
        this._US.openSnackBar("user deleted")
        this.getUsers()
      }
      else{
        this._US.openSnackBar("unable to delete user")
      }
    })
  }
}
