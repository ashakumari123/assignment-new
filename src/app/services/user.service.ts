import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  loginSubject = new Subject()
  
  constructor(private http:HttpClient,
    private _snackBar: MatSnackBar) { }

    loginUser(data){
      return this.http.post(environment.apiUrl+`login`,data)
    }

    getUsers(param){
      return this.http.get(environment.apiUrl+`users`,{params:param})
    }

    getSignleUser(id){
      return this.http.get(environment.apiUrl+`users/${id}`)
    }

    deleteUser(id){
      return this.http.delete(environment.apiUrl+`users/${id}`)
    }

    updateUser(id,data){
      return this.http.put(environment.apiUrl+`users/${id}`,data)
    }

    createUser(data){
      return this.http.post(environment.apiUrl+`users`,data)
    }

    openSnackBar(message:any) {
      this._snackBar.open(message,"", {
        duration: 500,
        horizontalPosition: "end",
        verticalPosition: "top",
      });
    }
  }

