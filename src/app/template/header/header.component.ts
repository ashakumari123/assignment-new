import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLogin:boolean = false 

  constructor(private _US:UserService,
    private router:Router) { }

  ngOnInit() {
    this._US.loginSubject.subscribe((res:any)=>{
      this.isLogin = true
    })
  }

  logout(){
    localStorage.removeItem('token')
    this._US.openSnackBar("log out successfully")
    this.isLogin = false
    this.router.navigate(['/'])
  }
}
